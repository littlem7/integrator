from integrator.tr_integrator import TrIntegrator


def func(x):
    return x + 1


def test_tr_integrator():
    a = -1
    b = 0
    x = 10
    print("a=%f, b=%f, x=%f, ca=%f" % (a, b, x, TrIntegrator.integrate(func, a, b, x)))
