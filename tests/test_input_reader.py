from integrator.input_reader import InputReader
import os

def test_input_reader():
    InputReader.read_from_file(os.path.dirname(os.path.abspath(__file__)) + "/data.txt")