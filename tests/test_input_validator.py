from integrator.input_validator import InputValidator


def test_input_validator():
    begin = 3
    end = 4
    x = 10
    # properly values
    InputValidator.validate_from_file(begin, end, x, [2.3, 3.2, 2.3])
    InputValidator.validate_from_file(begin, end, x)
    # swap checked
    InputValidator.validate_from_file(end, begin, x, [2, 3, 2])
    # begin bad type
    InputValidator.validate_from_file('begin', end, x, [2, 3, 2])
    # end bad type
    InputValidator.validate_from_file(begin, 'end', x, [2, 3, 2])
    # number of compartments bad type
    InputValidator.validate_from_file(begin, end, 'x', [2, 3, 2])
    # number of compartments out of range
    InputValidator.validate_from_file(begin, end, -7, [2, 3, 2])
    # coefficients good string type
    InputValidator.validate_from_file(1, 2, x, ['2', '2'])
    # coefficients bad type
    InputValidator.validate_from_file(1, 2, x, [('2', '2'), 3])