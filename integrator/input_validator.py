import re
from integrator.tr_integrator import TrIntegrator


class InputValidator:
    @staticmethod
    def validate_from_file(begin_data, end_data, x_data, args_data=[]):
        """
        :param begin_data: begin data to validate
        :param end_data: end data to validate
        :param x_data: x range data to validate
        :param args_data: list of coefficients to validate
        :return: list of validate (if possible) begin, end, x range, coefficients
                 and additional messages from validation (especially errors)
        """
        coefficients = ''
        val_msg = ' '
        val_msg += InputValidator.values_verification(begin_data, end_data,x_data, args_data)
        if val_msg == ' ':
            begin = float(begin_data)
            end = float(end_data)
            x = int(x_data)
            for i in range(len(args_data)):
                coefficients += str(float(args_data[i]))
                if i != len(args_data)-1:
                    coefficients += ' '
            val_msg += ' all values in file are correct '
            return [begin, end, x, coefficients, val_msg]
        else:
            return [begin_data, end_data, x_data, args_data, val_msg]

    @staticmethod
    def validate_from_gui(field_values, field_names):
        """
        :param field_values: contains list of values
        :param field_names: contains list of names of values
        :return: error message or score of integration polynomial function
        """
        error_msg = ""

        for i in range(len(field_names)):
            if field_values[i].strip() == "":
                error_msg += '"%s" is a required field.' % field_names[i]
                return error_msg

        begin = field_values[0]
        end = field_values[1]
        x = field_values[2]
        coefficients = []
        coefficients_data = re.split(' ', field_values[3])

        def polynomial_func(y):
            """
            :param y: variable
            :return: result of polynomial function with validated coefficients
            """
            result = 0
            for i in range(len(coefficients)):
                result += coefficients[i] * y ** (len(coefficients) - i - 1)
            return result

        def polynomial_func_to_string():
            """
            :return: nice representation of integrating polynomial function
            """
            function = ""
            for i in range(len(coefficients)):
                if coefficients[i] != 0.0:
                    c = '(' + str(coefficients[i]) + ')'
                    p = ''
                    if len(coefficients) - i - 1 > 1:
                        p = 'x^%d' % (len(coefficients) - i - 1)
                    elif len(coefficients) - i - 1 == 1:
                        p = 'x'
                    if p != '':
                        function += c + ' * ' + p
                    else:
                        function += c + p
                if i != len(coefficients) - 1 and (coefficients[i+1] != 0.0):
                    function += ' + '
            return function

        error_msg += InputValidator.values_verification(begin, end, x, coefficients_data)
        if error_msg == '':
            begin = float(begin)
            end = float(end)
            x = int(x)
            for arg in coefficients_data:
                coefficients.append(float(arg))
            return 'end of validation\n ∫' + polynomial_func_to_string() + ' dx = %f' % TrIntegrator.integrate(
                polynomial_func, begin, end, x)
        else:
            return error_msg

    @staticmethod
    def to_float(value_data):
        """
        :param value_data: value to verify (conversion to float)
        :return: True when value can be converted to float; False if not
        """
        try:
            float(value_data)
            return True
        except ValueError:
            return False
        except TypeError:
               return False

    @staticmethod
    def to_int(value_data):
        """
        :param value_data: value to verify (conversion to int)
        :return: True when value can be converted to int; False if not
        """
        try:
            int(value_data)
            return True
        except ValueError:
            return False
        except TypeError:
            return False

    @staticmethod
    def values_verification(begin_data, end_data, x_data, args_data):
        """
        :return: error message when one of parameters has wrong format
                     and empty string when everything is ok
        """
        if not InputValidator.to_float(begin_data):
            return 'must correct begin value'
        if not InputValidator.to_float(end_data):
            return 'must correct end value'
        if float(begin_data) > float(end_data):
            return 'begin value is greater than end value'
        if not InputValidator.to_int(x_data):
            return 'must correct number of compartments'
        if int(x_data) <= 0:
            return 'must correct range of number of compartments'
        for i in range(len(args_data)):
            if not InputValidator.to_float(args_data[i]):
                return 'must correct %d coefficient value' % (i + 1)
        return ''
