class TrIntegrator:
    @staticmethod
    def integrate(func, begin, end, x):
        """
        :param func: polynomial function
        :param begin: range start
        :param end: end of range
        :param x: number of compartments
        :return: result od integration
        """
        result = 0
        hop = (end - begin)/x
        for i in range(x):
            result += 0.5 * (func(begin + i*hop) + func(begin + (i+1)*hop)) * hop
        return result
