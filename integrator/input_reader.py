import re
from easygui import *
from integrator.input_validator import InputValidator


class InputReader:
    @staticmethod
    def read_from_file(file_name):
        """
        :param file_name: file from which method reads
        :return: list of validate (if possible) begin, end, x range, coefficients
                 and additional messages from validation and reading (especially errors)
        """
        file = open(file_name, 'r')
        lines = []
        for line in file:
            lines.append(line)
        file.close()
        file_msg = ''
        if len(lines) < 4:
            # bad file format, not enough lines
            file_msg += "bad file format; %d lines aren't enough" % len(lines)
            for i in range(len(lines), 4):
                lines.append(' ')
        else:
            file_msg += 'number of lines in your file is ok'
        val_input = InputValidator.validate_from_file(lines[0], lines[1], lines[2], re.split(' ', lines[3]))
        val_input[4] += file_msg
        return val_input

    @staticmethod
    def read_from_gui(field_values=['[float]', '[float]', '[int]', '[floats separated by spaces]'],
                      msg="Enter your values:"):
        """
        :param field_values: contains default types of values
        :param msg: message in multenterbox
        """
        title = "Integrator"
        field_names = ['Begin of integrating', 'End range of integrating', 'Δ (number of compartments)',
                       'Coefficients for powers']

        field_values = multenterbox(msg, title, field_names, field_values)
        while 1:
            if not field_values:
                break
            errmsg = InputValidator.validate_from_gui(field_values, field_names)
            field_values = multenterbox(errmsg, title, field_names, field_values)