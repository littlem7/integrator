from integrator.input_reader import InputReader
from easygui import *
import sys


class ApplicationMgr:
    @staticmethod
    def gui_application():
        InputReader.read_from_gui()

    @staticmethod
    def gui_from_file():
        title = "Select file."
        msg = "File to read, validate and integrate."
        file_to_read = fileopenbox(msg, title)
        input_from_file = InputReader.read_from_file(file_to_read)
        to_gui = []
        for i in range(4):
            to_gui.append(input_from_file[i])
        InputReader.read_from_gui(to_gui, input_from_file[4])

if __name__ == '__main__':
    if sys.argv[1] == 'from_file':
        ApplicationMgr.gui_from_file()
    else:
        ApplicationMgr.gui_application()
