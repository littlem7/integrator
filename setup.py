from setuptools import setup

setup(
    name='integrator',
    version='1.0',
    description='Integration polynomial functions',
    author='Anna Bogusz',
    author_email='anna.bogusz@fis.agh.edu.pl',
    packages=[
        'integrator',
    ],
    setup_requires=[
    	'pytest-runner',
    ],
    tests_require=[
    	'pytest'
    ],
    install_requires=[
        'pytest',
        'easygui',
    ],
)
