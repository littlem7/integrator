# Integrator
Program integrates polynomial functions.


## Using

```
#!python

make run
```
command causes program


```
#!python

make run_from_file
```
command causes program with input from file

data.txt exemplary file with well prepared data